# Summary
## Welcome 

## Veille
* [Cloud](veille/cloud/cloud.md)
  * [AWS](veille/cloud/aws/introduction.md)
    * [Introduction](veille/cloud/aws/introduction.md)
  * [Azure](veille/cloud/azure/introduction.md)
    * [Introduction](veille/cloud/aws/introduction.md)
*  [IA / ML](veille/artificial_intelligence/artificial_intelligence.md)
  * [Usages pour activités malveillantes](veille/artificial_intelligence/ia_activites_malveillantes.md)
*  [Red Team](veille/redteam/redteam.md)
  * [Reconnaissance](veille/redteam/reconnaissance/reconnaissance.md)


## Projets d'études
* [Phishing](projets/phishing/phishing.md)

<!-- ## Procédures -->

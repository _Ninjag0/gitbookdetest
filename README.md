# Gitbook de la Purple Team

Bienvenue dans ce GitBook dédié à l'équipe Purple Team ! 

Ici, nous avons créé un espace centralisé pour explorer et approfondir nos sujets de recherche. En tant que membre de la Purple Team, nous sommes convaincus que la collaboration et l'échange sont essentiels pour résoudre les défis les plus complexes. Nous avons donc créé cet espace pour partager nos connaissances, nos idées et nos expériences avec l'ensemble de l'équipe. 

Nous tenons également à souligner que cet espace n'a pas pour but de remplacer [GitLab](https://git.gatewatcher.com/), qui reste notre outil principal de collaboration et de gestion de code. Au contraire, ce GitBook est complémentaire à GitLab et vise à offrir un espace dédié et privatisé pour la rédaction et la centralisation de nos recherches.





![purpleteam](./imgs/purple.png)
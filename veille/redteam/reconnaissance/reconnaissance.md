# Veille Reconnaissance externe

[TOC]

## Introduction

### Définition

La phase de reconnaissance est généralement l'**une des premières actions pendant une opération Red Team**. 

Ci-dessous un schéma reprenant les grandes étapes de ce type de mission. 



![undefined](./img/Intrusion_Kill_Chain_-_v2.png)



Au-delà d'être la première étape, la reconnaissance est une **étape clé** dans une opération de red team. Elle consiste à **recueillir des informations sur la cible ou l'environnement à cibler afin de mieux comprendre sa structure, ses vulnérabilités et ses systèmes**. Les red teamers peuvent utiliser diverses techniques pour recueillir ces informations, telles que l'analyse de documents publics, la recherche de vulnérabilités connues, la collecte d'informations sur les réseaux sociaux ou les forums en ligne, etc.

Cette étape est cruciale car elle permet à la red team de mieux comprendre les systèmes et les processus de l'entreprise ciblée, ce qui peut aider à identifier les vulnérabilités et les points faibles qui pourraient être exploités lors d'une attaque. Cela peut également aider la red team à comprendre comment les défenses de l'entreprise peuvent être contournées ou évitées.

En résumé, l'étape de reconnaissance est essentielle pour une opération de red team réussie, car elle fournit des **informations précieuses qui peuvent aider à planifier et à exécuter une attaque efficace**.



### Catégories de reconnaissance

À noter que la reconnaissance peut être divisée en **deux grandes catégories** : la **reconnaissance passive** et la **reconnaissance active**:

\- reconnaissance passive : n'interagissant pas directement avec la cible (que ce soit l'infrastructure ou les personnes);
_exemple:_ en utilisant un moteur de recherche pour se renseigner sur les activités de l'entreprise.

\- reconnaissance active : sondage des infrastructures et des personnes, reconnaissances des ports et services exposés, découvertes des sites web et des vulnérabilités potentiellement présentes.
_exemple:_ vérifier sur un serveur appartenant à l'entreprise si un port est ouvert ou non

*NB: On démarre essentiellement avec une reconnaissance passive puis on passe à de la reconnaissance active uniquement si nécessaire.*



### Type de données collectées

#### Reconnaissance Passive

* Moteurs de recherche : Google, Bing, Yandex, Archives

  Point d'entrée pour obtenir de nombreuses informations de toutes natures mais aussi pour filtrer sur les résultats.

  

* Réseaux sociaux (Perso / Pro) : Facebook, Twitter, Instagram, Snapchat, Linkedin, Google Networks


​	Veille des réseaux sociaux pour se renseigner sur les employés mais aussi sur la structure. 



* Communautés en ligne : Blogs, Forums tech, Reddit, Discord


Veille sur les sites techniques où les collaborateurs aurait pu publier des informations techniques pertinentes. 



* Adresses emails

  * adresses professionnelles valides
  * à défaut nomenclature employée

Obtention des adresses mails des collaborateurs ou identification du pattern utilisé. 



* Nom utilisateurs / identifiants pro

Identifier des noms d'utilisateurs valides pour se connecter à des services précis de l'entreprise. 



* Moteurs de recherches pour personnes

Veille des moteurs de recherches dédiés aux personnes pour centraliser des données personnelles, tel que numéro de téléphone ou adresses. 



* Numéros de téléphone

Identifier des numéros de téléphones d'employés, peut être particulièrement utile pour les fraudes au président. 



* Online Maps

Identifier le lieu géographique et les systèmes de protection en place (ex:caméra)



* Documents  / Images / Videos

Au delà des informations sensibles qu'ils peuvent contenir, on peut s’intéresser à l'ensemble des méta data. 



* Nom de domaines

Retrouver l'ensemble des domaines et sous-domaines, ainsi que tous les informations les concernant.



* Adresses Ip

Retrouver l'ensemble des IPs appartenant à la cible, ainsi que tous les informations les concernant.



* Documents administratifs et commerciaux

Trouver des informations financières et administratives sur la structure pour comprendre à qui nous avons à faire. 



* Fuites de données

Rechercher dans les leaks des comptes fonctionnels, mais aussi toutes autres informations utiles. 



#### Reconnaissance Active

- DNS
  - Transfert de zone
  - Reverse
  - Bruteforce 

- Ping sweep and ports scanning

  - Service and Os enumeration

- SNMP sweep

- SMTP bounce back

- Énumération des services / Découverte des OS

- Sites webs

  - Composants et versions
  - Certificats
  - Crawling
  - Panel



## Effectuer une reconnaissance

Nous nous intéressons dans cette section uniquement à la méthodologie qui peut être appliquée pour la reconnaissance active et en particulier à la cartographie des actifs d'un SI et aux moyens qui peuvent être mis en œuvre pour les cartographier. 

### ASN

<u>Objectif :</u> identifier une ou des plage(s) d'adresses ip dédiées. 

Un numéro de système autonome (ASN) est un numéro unique attribué à un système autonome (AS) par l'Internet Assigned Numbers Authority (IANA).

Un AS est constitué de blocs d'adresses IP qui ont une politique d'accès aux réseaux externes bien définie et sont administrés par une seule organisation mais peuvent être composés de plusieurs opérateurs. Il est donc intéressant de savoir si l'entreprise ciblée dispose d'un ASN.

Il est possible d' effectuer une recherche par nom d'entreprise, par adresse IP ou par domaine sur https://bgp.he.net/.

Selon la région de l'entreprise, ces liens peuvent être utiles pour recueillir plus de données : [AFRINIC](https://www.afrinic.net/) (Afrique), [Arin](https://www.arin.net/about/welcome/region/) (Amérique du Nord), [APNIC](https://www.apnic.net/) (Asie), [LACNIC](https://www.lacnic.net/) (Amérique latine), [RIPE NCC](https://www.ripe.net/) (Europe).

Vous pouvez trouver les plages d'adresses IP d'une organisation en utilisant également http://asnlookup.com/ (il a une API gratuite).

Ci-dessous un exemple pour la société Gatewatcher et la société Ubisoft :

**Gatewatcher**

```bash
https://bgp.he.net/ --> 0 results
https://www.ripe.net/ --> 0 results
```

***TIPS :*** si pas d'ASN, un premier point d'entrée peut être de s’intéresser au site vitrine de la société, et de résoudre le nom de domaine ainsi que de faire un whois pour collecter quelques premières informations. 

```bash
nslookup gatewatcher.com 8.8.8.8

Server:		8.8.8.8
Address:	8.8.8.8#53

Non-authoritative answer:
Name:	gatewatcher.com
Address: 104.19.154.92
```

**Ubisoft**

```
https://bgp.he.net/

Result	Description
ubisoft	
AS8361	Ubisoft International SASFrance
AS55743	UBISOFT EntertainmentHong Kong
AS49078	SC UBISOFT SRLRomania
AS28759	Ubisoft GmbHGermany
AS22634	Ubisoft EntertainmentCanada
AS131553	Ubisoft ChengduChina
AS131484	Ubisoft ShanghaiChina
2a04:7140:21::/48	Ubisoft International SASFrance
2406:d140:8000::/34	Ubisoft ShanghaiChina
216.98.48.0/20	Ubisoft EntertainmentCanada
212.104.205.0/24	Ubisoft International SASFrance
212.104.204.0/24	Ubisoft International SASFrance
212.104.202.0/24	Ubisoft International SASFrance
212.104.201.0/24	Ubisoft International SASFrance
212.104.200.0/24	Ubisoft International SASFrance
212.104.192.0/20	Ubisoft International SASFrance
195.88.183.0/24	SC UBISOFT SRLRomania
195.88.182.0/24	SC UBISOFT SRLRomania
195.22.144.0/23	Ubisoft International SASFrance
194.2.155.0/24	Ubisoft International SASFrance
194.169.249.0/24	Ubisoft International SASFrance
193.138.66.0/24	Ubisoft GmbHGermany
185.38.20.0/22	Ubisoft International SASFrance
130.254.86.0/23	Ubisoft EntertainmentCanada
130.254.84.0/23	Ubisoft EntertainmentCanada
130.254.82.0/23	Ubisoft EntertainmentCanada
130.254.80.0/23	Ubisoft EntertainmentCanada
130.254.64.0/19	Ubisoft EntertainmentCanada
103.159.122.0/23	Ubisoft ShanghaiChina
```

De nombreux ASN ont pu être identifiés.

### Domaines

<u>Objectifs :</u>  rechercher le ou les **domaines principaux** de chaque entreprise ciblée. 

#### Reverse DNS

Dans l'hypothèse où nous avez trouvé toutes les plages d'adresses IP appartenant à l'entreprise, nous pouvons essayer d'effectuer **des recherches DNS inversées** sur ces **adresses IP pour trouver plus de domaines** .  Nous pouvons utiliser un serveur DNS appartenant à la victime ou un serveur DNS bien connu (ex: 8.8.8.8).

Pour que cela fonctionne, l'administrateur doit activer manuellement le PTR. Vous pouvez également utiliser un outil en ligne pour cette information : http://ptrarchive.com/.

```bash
dnsrecon -r <DNS Range> -n <IP_DNS>   #DNS reverse of all of the addresses
dnsrecon -r 185.45.4.0/24 -n 8.8.8.8 #Using google dns
```

Ci-dessous un exemple avec Ubisoft :

*NB : Nous ne pouvons pas le faire pour Gatewatcher car nous n'avons pas identifier de range d'ips.* 

```bash
H@ckila 
[~/TOOLS/dnsrecon] (master) 🦅 ❯❯  dnsrecon -r 185.38.20.0/22 -n 8.8.8.8      
./dnsrecon.py:292: DeprecationWarning: please use dns.resolver.Resolver.resolve() instead
  answers = res.query(test_name, record_type, tcp=True)
[*] Reverse Look-up of a Range
[*] Performing Reverse Lookup from 185.38.20.0 to 185.38.23.255
[*] 	 PTR lb-prod-tc-pdc-detect02.ubisoft.com 185.38.20.87
[*] 	 PTR lb-prod-tc-pdc-traversal01.ubisoft.com 185.38.20.86
[*] 	 PTR lb-prod-tc-pdc-detect01.ubisoft.com 185.38.20.86
[*] 	 PTR mail.panzergeneral.com 185.38.20.159
[*] 	 PTR staging.massive.se 185.38.20.162
[*] 	 PTR pdc-lb-rdv-prod02.ubisoft.com 185.38.21.5
[*] 	 PTR lb-uat-traversal01.ubisoft.com 185.38.21.8
[*] 	 PTR lb-uat-detect01.ubisoft.com 185.38.21.8
[*] 	 PTR lb-prod-tc2-traversal01.ubisoft.com 185.38.21.8
[*] 	 PTR lb-prod-tc2-detect01.ubisoft.com 185.38.21.8
[*] 	 PTR lb-prod-tc2-detect02.ubisoft.com 185.38.21.9
[*] 	 PTR lb-uat-detect02.ubisoft.com 185.38.21.9
[*] 	 PTR lb-pdc-rdv-uat.ubisoft.com 185.38.21.16
[*] 	 PTR forums-emea.ubisoft.com 185.38.21.25
[*] 	 PTR lb-rdv-tgt-pdc-uat.ubisoft.com 185.38.21.30
[*] 	 PTR lb-pdc-r6.ubisoft.com 185.38.21.39
[*] 	 PTR pdc-onl-mail-out04.ubisoft.com 185.38.21.53
[*] 	 PTR pdc-onl-mail-out02.ubisoft.com 185.38.21.51
[*] 	 PTR pdc-onl-mail-out06.ubisoft.com 185.38.21.55
[*] 	 PTR pdc-onl-mail-out05.ubisoft.com 185.38.21.54
[*] 	 PTR pdc-onl-mail-out03.ubisoft.com 185.38.21.52
[*] 	 PTR pdc-vm-rdv01.ubisoft.com 185.38.21.80
[*] 	 PTR pdc-vm-rdv02.ubisoft.com 185.38.21.81
[*] 	 PTR pdc-vm-rdv04.ubisoft.com 185.38.21.83
[*] 	 PTR pdc-vm-rdv03.ubisoft.com 185.38.21.82
[*] 	 PTR pdc-vm-rdv05.ubisoft.com 185.38.21.84
[*] 	 PTR pdc-vm-rdv06.ubisoft.com 185.38.21.85
[*] 	 PTR pdc-dmz-ras08.vpn.ubisoft.com 185.38.21.196
[*] 	 PTR pdc-dmz-ras09.vpn.ubisoft.com 185.38.21.197
[*] 	 PTR emea-partner.vpn.ubisoft.com 185.38.21.198
[*] 	 PTR emea.vpn.ubisoft.com 185.38.21.198
[*] 	 PTR pdc-dmz-ras06.vpn.ubisoft.com 185.38.21.199
[*] 	 PTR pdc-dmz-ras07.vpn.ubisoft.com 185.38.21.200
[*] 	 PTR pdc-dmz-ras01.vpn.ubisoft.com 185.38.21.201
[+] 34 Records Found
```

Une capture est disponible [ici](#reverse-dns). 

#### Reverse Whois

À l'intérieur d'un **whois** , nous pouvons trouver de nombreuses **informations intéressantes** telles que **le nom de l'organisation** , **l'adresse** , **les e-mails** , les numéros de téléphone, etc. Mais ce qui est encore plus intéressant, c'est que nous pouvons trouver **plus d'actifs liés à une entreprise** . Si nous effectuons **des recherches whois inversées par l'un des ces champs** (par exemple d'autres registres whois où le même e-mail apparaît). Nous pouvons utiliser des outils en ligne comme :

- [https://viewdns.info/reversewhois/](https://viewdns.info/reversewhois/) - **Gratuit**
- [https://domaineye.com/reverse-whois](https://domaineye.com/reverse-whois) - **Gratuit**
- [https://www.reversewhois.io/](https://www.reversewhois.io) - **Gratuit**
- [https://www.woxy.com/](https://www.whoxy.com) - Web **gratuit , pas d'API gratuite.**
- [http://reversewhois.domaintools.com/](http://reversewhois.domaintools.com) - Pas gratuit
- [https://drs.whoisxmlapi.com/reverse-whois-search](https://drs.whoisxmlapi.com/reverse-whois-search) - Pas gratuit (seulement **100** recherches gratuites)
- [https://www.domainiq.com/](https://www.domainiq.com) - Pas gratuit

Il est possible d'automatiser cette tâche à l'aide de [**DomLink** ](https://github.com/vysecurity/DomLink) (nécessite une clé API whoxy)

Il est également possible d'effectuer cette découverte whois inversée automatique avec [amass](https://github.com/OWASP/Amass) :

```bash
H@ckila 
[~/Documents/VEILLE/RECONNAISSANCE] 🦅 ❯❯  docker run -v OUTPUT_DIR_PATH:/.config/amass/ caffix/amass intel -d gatewatcher.com -whois
performive.com
properties.live
garagegymproducts.com
800goldlaw.com
cavonispizza.com
shipshewanaontheroad.com
getterfarms.com
zupreem.com
earlyedstation.com
elixircreative.com
pimpleprincess.com
buyexpireds.com
heritagedefense.org
rocket.net
icycanada.com
icytales.com
autumntrailsvet.com
ncinjurylawyer.net
fitdadchris.com
icondentalcenter.com
kerrypicket.com
kcbellanapoli.com
therichvaldes.com
motegahealth.com
twodot.marketing
thegreggjarrett.com
hannity.com
thejeffreylord.com
growdigitally.com
m3mediamanagement.com
dealvwant.com
accessiblevacations.org
jntechnix.com
newburgfirerescue.com




```

> Chaque fois qu'un nouveau nom de domaine est identifié cette recherche doit être effectuée. 
>
> 

#### Trackeurs

Si l'on trouve le **même ID du même tracker** sur 2 pages différentes nous pouvons supposer que **les deux pages** sont **gérées par la même équipe**. Par exemple, si vous voyez le même **identifiant Google Analytics** ou le même **identifiant Adsense** sur plusieurs pages.

Il existe des pages et des outils qui permettent d'effectuer cette recherche  :

- **[Udon](https://github.com/dhn/udon)**
- <u>**[BuiltWith](https://builtwith.com)**</u>
- <u>**[Sitesleuth](https://www.sitesleuth.io)**</u>
- <u>**[Publicwww](https://publicwww.com)**</u>
- <u>**[SpyOnWeb](http://spyonweb.com)**</u>



#### **Favicon**

Il est possible de retrouver des domaines ou des sous-domaines liés à une cible en recherchant le même hash de favicon. 

L'outil ][favihash]() (https://github.com/m4ll0k/BBTz/blob/master/favihash.py) permet notamment de faire ça. 

Il est également possible de s'appuyer sur shodan pour faire une recherche par favicon. 



#### Copyright

Recherchez des chaînes de caractères dans les pages Web qui pourraient être partagées entre différents sites Web au sein de la même organisation. 

La **chaîne de copyright** pourrait être un bon exemple. On peut ensuite effectuer une recherche via les moteurs de recherche ou sur shodan. 



#### CRT Time

https://swarm.ptsecurity.com/discovering-domains-via-a-time-correlation-attack/



### Sous domaines

<u>Objectifs :</u>  rechercher tous les sous-domaines possibles de chaque domaine trouvé. 



#### Transfert de zone

Le **transfert de zone DNS** également connu de son [opcode](https://fr.wikipedia.org/wiki/Opcode) [mnémotechnique](https://fr.wikipedia.org/wiki/Mnémotechnique) **AXFR**, est un type de transaction [DNS](https://fr.wikipedia.org/wiki/Domain_Name_System). C'est l'un des nombreux mécanismes disponibles pour répliquer les bases de données distribuées contenant les données DNS au travers d'un ensemble de serveurs DNS. Le transfert de zone peut être effectué de deux manières différentes : le transfert de zone complet (AXFR) ou le transfert de zone incrémental (IXFR).

Ci-dessous un exemple avec ubisoft

```bash
H@ckila 
[~] 🦅 ❯❯ dig axfr @195.22.144.124 ubisoft.fr                

; <<>> DiG 9.16.1-Ubuntu <<>> axfr @195.22.144.124 ubisoft.fr
; (1 server found)
;; global options: +cmd
; Transfer failed.

```

Une capture est disponible ici. 



#### Bruteforce DNS

Effectuer un bruteforce dns sur chaque domaine identifié a l'aide d'un dictionnaire dans l'espoir d'en découvrir des valides. 

Voici une liste de wordlists : 

- https://gist.github.com/jhaddix/86a06c5dc309d08580a018c66354a056
- https://wordlists-cdn.assetnote.io/data/manual/best-dns-wordlist.txt
- https://localdomain.pw/subdomain-bruteforce-list/all.txt.zip
- https://github.com/pentester-io/commonspeak
- https://github.com/danielmiessler/SecLists/tree/master/Discovery/DNS



Un outil qui peut être utilisé est [massdns](https://github.com/blechschmidt/massdns), il en existe de nombreux autres. 



Ci-dessous un exemple avec Ubisoft :

```bash
dnsrecon -d ubisoft.fr -D ~/Documents/WORDLISTS/SecLists/Discovery/DNS/deepmagic.com-prefixes-top500.txt -t brt
```

Une capture est disponible ici. 



#### Bruteforce SCRS

Parfois, certaines pages web ne renvoient que l'en-tête ***Access-Control-Allow-Origin\*** lorsqu'un domaine/sous-domaine valide est défini dans l' en-tête ***Origin\*** . Dans ce cas, il est possible d'abuser de ce comportement pour **découvrir** de nouveaux **sous-domaines**.

Voici un exemple de commande pour exploiter ce type de vulnérabilité  :

```
ffuf -w subdomains-top1million-5000.txt -u http://10.10.10.10 -H 'Origin: http://FUZZ.gatewatcher.com' -mr "Access-Control-Allow-Origin" -ignore-body
```



### Adresses IPs

<u>Objectifs :</u>  Collecter l'ensemble des adresses ip de entreprise ciblée. 

Grâce aux étapes précédentes nous avons trouvé  des plages d'adresses IP, des domaines et des sous-domaines. A partir de ces éléments nous nous assurons pour chacun d'avoir l'ensemble des IPs correspondantes et nous dressons un tableau exhaustif.  



### Serveurs Web

<u>objectifs :</u> Découvrir l'ensemble des serveurs web dans le périmètre ciblé. 

Il est possible de s'appuyer sur masscan ou des outils comme httprobe ou httpx en ne ciblant que des ports connus pour être dédiés à des services web.



## Captures Réseaux (Pcap)

### Reverse DNS

nom pcap : **reverse_dns_ubisoft.pcap**

Scénario : L'attaquant effectue un reverse DNS sur un range d'ip appartenant à sa cible et se base sur les dns google ici. 

- IP attaquante : 192.168.1.39
- range d'ips: 185.38.20.0/22
- serveur dns utilisé : 8.8.8.8

commande lancée : 

```bash
dnsrecon -r 185.38.20.0/22 -n 8.8.8.8 
```

Réalisme :  Attaque réaliste.

Durée de la capture : 4.15 secondes.



### Transfert de zone

nom pcap : **zone_transfer_failed.pcap**

Scénario : L'attaquant effectue un transfert de zone sur sa victime: la demande échoue. 

- IP attaquante : 192.168.1.39
- IP du serveur DNS attaqué : 195.22.144.124

commande lancée : 

```bash
dig axfr @195.22.144.124 ubisoft.fr
```

Réalisme :  Attaque réaliste mais de moins en moins courante sur les DNS connus car ils sont souvent bien configurés. 

Durée de la capture : 4.39 secondes.



nom pcap : **zone_transfer_succeed.pcap**

Scénario : L'attaquant effectue un transfert de zone sur sa victime: la demande est acceptée. 

- IP attaquante : 192.168.1.39
- IP du serveur DNS attaqué : 195.22.144.124

commande lancée : 

```bash
dig @challenge01.root-me.org -p 54011 AXFR ch11.challenge01.root-me.org
```

Réalisme :  Attaque réaliste mais de moins en moins courante sur les DNS connus car ils sont souvent bien configurés. 

Durée de la capture : 12 secondes.



### Bruteforce DNS

#### 500 entrées

nom pcap : **domains_bruteforce_ubisoft.fr-500_entries.pcap**

Scénario : l'attaquant effectue une énumération de sous-domaines possibles en tentant de résoudre des nom de domaines probables à partir d'un dictionnaire. Ce dictionnaire est issu du dépot git [SecLists](https://github.com/danielmiessler/SecLists) et contient 500 entrées. 

- IP attaquante : 192.168.1.39
- IP du serveur DNS attaqué : 195.22.144.124

commande lancée : 

```bash
dnsrecon -d ubisoft.fr -D ~/Documents/WORDLISTS/SecLists/Discovery/DNS/deepmagic.com-prefixes-top500.txt -t brt
```

Réalisme :  Attaque réaliste et courante, génère beaucoup de bruit au niveau du dns. 

Durée de la capture : 150 secondes.

#### 5000 entrées

nom pcap : **domains_bruteforce_ubisoft.fr-5000_entries.pcap**

Scénario : l'attaquant effectue une énumération de sous-domaines possibles en tentant de résoudre des nom de domaines probables à partir d'un dictionnaire. Ce dictionnaire est issu du dépot git [SecLists](https://github.com/danielmiessler/SecLists) et contient 5000 entrées. 

- IP attaquante : 192.168.1.39
- IP du serveur DNS attaqué : 195.22.144.124

commande lancée : 

```bash
dnsrecon -d ubisoft.fr -D ~/Documents/WORDLISTS/SecLists/Discovery/DNS/subdomains-top1million-5000.txt -t brt
```

Réalisme :  Attaque réaliste et courante, génère beaucoup de bruit au niveau du dns. 

Durée de la capture : 780 secondes / 13 minutes.



#### Scan de ports



#### Crawling site web

nom pcap : **http_crawling_website.pcap**

Scénario : un attaquant ou un robot effectue le crawling d'un site web, exemple ici avec https://juice-shop.herokuapp.com/. 

commande lancée : Burpsuite en mode crawling sans scan. 

Réalisme : Très réaliste. 

Durée de la capture : 5000 secondes / 83 minutes.



#### Scanning site web

nom pcap : **http_scanning_website.pcap**

Scénario : un attaquant ou un robot effectue un scan automatisé sur un site web, exemple ici avec ctf14.root-me.org. 

commande lancée : utilisation de l'outil nikto. 

```bash
H@ckila 
[~/TOOLS/nikto/program] (nikto-2.5.0) 🦅 ❯❯ ./nikto.pl -h http://ctf14.root-me.org
- Nikto v2.5.0
---------------------------------------------------------------------------
+ Target IP:          212.83.175.138
+ Target Hostname:    ctf14.root-me.org
+ Target Port:        80
+ Start Time:         2023-04-13 11:51:32 (GMT2)
---------------------------------------------------------------------------
+ Server: Apache/2.2.8 (Ubuntu) DAV/2
+ /: Retrieved x-powered-by header: PHP/5.2.4-2ubuntu5.10.
+ /: The anti-clickjacking X-Frame-Options header is not present. See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
+ /: The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type. See: https://www.netsparker.com/web-vulnerability-scanner/vulnerabilities/missing-content-type-header/
+ /index: Uncommon header 'tcn' found, with contents: list.
+ /index: Apache mod_negotiation is enabled with MultiViews, which allows attackers to easily brute force file names. The following alternatives for 'index' were found: index.php. See: http://www.wisec.it/sectou.php?id=4698ebdc59d15,https://exchange.xforce.ibmcloud.com/vulnerabilities/8275
+ Apache/2.2.8 appears to be outdated (current is at least Apache/2.4.54). Apache 2.2.34 is the EOL for the 2.x branch.
+ /: Web Server returns a valid response with junk HTTP methods which may cause false positives.
+ /: HTTP TRACE method is active which suggests the host is vulnerable to XST. See: https://owasp.org/www-community/attacks/Cross_Site_Tracing
+ /phpinfo.php: Output from the phpinfo() function was found.
+ /doc/: Directory indexing found.
+ /doc/: The /doc/ directory is browsable. This may be /usr/doc. See: http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1999-0678
+ /?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000: PHP reveals potentially sensitive information via certain HTTP requests that contain specific QUERY strings. See: OSVDB-12184
+ /?=PHPE9568F36-D428-11d2-A769-00AA001ACF42: PHP reveals potentially sensitive information via certain HTTP requests that contain specific QUERY strings. See: OSVDB-12184
+ /?=PHPE9568F34-D428-11d2-A769-00AA001ACF42: PHP reveals potentially sensitive information via certain HTTP requests that contain specific QUERY strings. See: OSVDB-12184
+ /?=PHPE9568F35-D428-11d2-A769-00AA001ACF42: PHP reveals potentially sensitive information via certain HTTP requests that contain specific QUERY strings. See: OSVDB-12184
+ /phpMyAdmin/changelog.php: phpMyAdmin is for managing MySQL databases, and should be protected or limited to authorized hosts.
+ /phpMyAdmin/ChangeLog: Server may leak inodes via ETags, header found with file /phpMyAdmin/ChangeLog, inode: 92462, size: 40540, mtime: Tue Dec  9 18:24:00 2008. See: http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2003-1418
+ /phpMyAdmin/ChangeLog: phpMyAdmin is for managing MySQL databases, and should be protected or limited to authorized hosts.
+ /test/: Directory indexing found.
+ /test/: This might be interesting.
+ /phpinfo.php: PHP is installed, and a test script which runs phpinfo() was found. This gives a lot of system information. See: CWE-552
+ /icons/: Directory indexing found.
+ /icons/README: Apache default file found. See: https://www.vntweb.co.uk/apache-restricting-access-to-iconsreadme/
+ /phpMyAdmin/: phpMyAdmin directory found.
+ /phpMyAdmin/Documentation.html: phpMyAdmin is for managing MySQL databases, and should be protected or limited to authorized hosts.
+ /phpMyAdmin/README: phpMyAdmin is for managing MySQL databases, and should be protected or limited to authorized hosts. See: https://typo3.org/
+ 8856 requests: 0 error(s) and 26 item(s) reported on remote host
+ End Time:           2023-04-13 11:52:57 (GMT2) (85 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested
```

Réalisme : Très réaliste. 

Durée de la capture : 100 secondes / 1.60 minute.

#### Brute force site web

nom pcap : **http_bruteforce_panel_dvwa.pcap**

Scénario : l'attaquant effectue une tentative de brute force sur une mire d'authentification. 

- IP attaquante : 192.168.1.57
- Adresse du site web attaquée : ctf14.root-me.org

commande lancée : 

```bash
wfuzz -c  -w ~/Documents/WORDLISTS/SecLists/Passwords/darkweb2017-top1000.txt -d "username=admin&password=FUZZ&Login=Login" -Z --sc 200 http://ctf14.root-me.org/dvwa/login.php
********************************************************
* Wfuzz 2.4.5 - The Web Fuzzer                         *
********************************************************

Target: http://ctf14.root-me.org/dvwa/login.php
Total requests: 999

===================================================================
ID           Response   Lines    Word     Chars       Payload                                                                     ===================================================================

Total time: 9.228548
Processed Requests: 999
Filtered Requests: 999
Requests/sec.: 108.2510
```

Réalisme :  Attaque réaliste et courante, génère beaucoup de bruit sur le serveur web ciblé. 

Durée de la capture : 10 secondes.



## Analyse Gcenter

Gcap : `gcap-dev-64-aha`

Gcenter : `10.2.19.104`

### Reverse DNS

La capture `reverse_dns_ubisoft.pcap` a été rejoué sur le Gcenter.

Pour le besoin de l'étude, l'ip 192.168.1.39 a été considérée comme ip externe. De la même façon le serveur dns 8.8.8.8 a été considéré comme interne lors du rejeu. 

Aucune alerte n'a été observée. 

![image-20230412160823568](./img/dns_reverse_gcenter_alert.png)

![dns_reverse_gcenter_kibana_metadata](./img/dns_reverse_gcenter_kibana_metadata.png)

![image-20230419094014696](./img//image-20230419094014696.png)

![reverse_dns_gcenter_kibana_ex_request](./img/reverse_dns_gcenter_kibana_ex_request.png)



#### Transfert de zone

##### Failed

nom pcap : **zone_transfer_failed.pcap**

Scénario : L'attaquant effectue un transfert de zone sur sa victime: la demande échoue. 

- IP attaquante : 192.168.1.39
- IP du serveur DNS attaqué : 195.22.144.124

commande lancée : 

```bash
dig axfr @195.22.144.124 ubisoft.fr
```

Réalisme :  Attaque réaliste mais de moins en moins courante sur les DNS connus car ils sont souvent bien configurés. 

Durée de la capture : 4.39 secondes.



Pour le besoin du rejeu, nous avons configuré le gcenter pour que 192.168.1.39 soit considérée comme une ip externe et que le serveur dns 195.22.144.124 soit considéré comme interne.

![dns_axfr_failed_gcenter_alert](./img/dns_axfr_failed_gcenter_alert.png)

![dns_axfr_failed_metadata_dns](./img/dns_axfr_failed_metadata_dns.png)

![dns_axfr_failed_dns_rrtype_252](./img/dns_axfr_failed_dns_rrtype_252.png)

La liste des codes est consultables [ici.](https://fr.wikipedia.org/wiki/Liste_des_enregistrements_DNS)![dns_axfr_failed_dns_refused](./img/dns_axfr_failed_dns_refused.png) 

On constate dans la réponse que la demande est refusée de la part du serveur DNS. 

##### Succeed

nom pcap : **zone_transfer_succeed.pcap**

Scénario : L'attaquant effectue un transfert de zone sur sa victime: la demande est acceptée. 

- IP attaquante : 192.168.1.39
- IP du serveur DNS attaqué : 195.22.144.124

commande lancée : 

```bash
dig @challenge01.root-me.org -p 54011 AXFR ch11.challenge01.root-me.org
```

Réalisme :  Attaque réaliste mais de moins en moins courante sur les DNS connus car ils sont souvent bien configurés. 

Durée de la capture : 12 secondes.



![dns_axfr_failed_gcenter_alert](./img/dns_axfr_failed_gcenter_alert.png)

Rien dans les metadata.

nom pcap : **domains_bruteforce_ubisoft.fr-5000_entries.pcap**

Scénario : l'attaquant effectue une énumération de sous-domaines possibles en tentant de résoudre des nom de domaines probables à partir d'un dictionnaire. Ce dictionnaire est issu du dépot git [SecLists](https://github.com/danielmiessler/SecLists) et contient 5000 entrées. 

- IP attaquante : 192.168.1.39
- IP du serveur DNS attaqué : 195.22.144.124

commande lancée : 

```bash
dnsrecon -d ubisoft.fr -D ~/Documents/WORDLISTS/SecLists/Discovery/DNS/subdomains-top1million-5000.txt -t brt
```

Réalisme :  Attaque réaliste et courante, génère beaucoup de bruit au niveau du dns. 

Durée de la capture : 780 secondes / 13 minutes.

![dns_bruteforce__gcenter_alert](./img/dns_bruteforce__gcenter_alert.png)



![dns_bruteforce_metadata](./img/dns_bruteforce_metadata.png)



#### Scan de ports



#### Crawling site web

nom pcap : http_crawling_website.pcap

Scénario : un attaquant ou un robot effectue le crawling d'un site web, exemple ici avec https://juice-shop.herokuapp.com/. 

commande lancée : Burpsuite en mode crawling sans scan. 

Réalisme : Très réaliste. 

Durée de la capture : 5000 secondes / 83 minutes.

![http_crawling_metadata](./img/http_crawling_metadata.png)

![http_crawling_metadata_2](./img/http_crawling_metadata_2.png)

![http_crawling_metadata_requetes](./img/http_crawling_metadata_requetes.png)

#### Scanning site web

nom pcap : **http_scanning_website.pcap**

Scénario : un attaquant ou un robot effectue un scan automatisé sur un site web, exemple ici avec ctf14.root-me.org. 

commande lancée : utilisation de l'outil nikto. 

```bash
H@ckila 
[~/TOOLS/nikto/program] (nikto-2.5.0) 🦅 ❯❯ ./nikto.pl -h http://ctf14.root-me.org
- Nikto v2.5.0
---------------------------------------------------------------------------
+ Target IP:          212.83.175.138
+ Target Hostname:    ctf14.root-me.org
+ Target Port:        80
+ Start Time:         2023-04-13 11:51:32 (GMT2)
---------------------------------------------------------------------------
+ Server: Apache/2.2.8 (Ubuntu) DAV/2
+ /: Retrieved x-powered-by header: PHP/5.2.4-2ubuntu5.10.
+ /: The anti-clickjacking X-Frame-Options header is not present. See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
+ /: The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type. See: https://www.netsparker.com/web-vulnerability-scanner/vulnerabilities/missing-content-type-header/
+ /index: Uncommon header 'tcn' found, with contents: list.
+ /index: Apache mod_negotiation is enabled with MultiViews, which allows attackers to easily brute force file names. The following alternatives for 'index' were found: index.php. See: http://www.wisec.it/sectou.php?id=4698ebdc59d15,https://exchange.xforce.ibmcloud.com/vulnerabilities/8275
+ Apache/2.2.8 appears to be outdated (current is at least Apache/2.4.54). Apache 2.2.34 is the EOL for the 2.x branch.
+ /: Web Server returns a valid response with junk HTTP methods which may cause false positives.
+ /: HTTP TRACE method is active which suggests the host is vulnerable to XST. See: https://owasp.org/www-community/attacks/Cross_Site_Tracing
+ /phpinfo.php: Output from the phpinfo() function was found.
+ /doc/: Directory indexing found.
+ /doc/: The /doc/ directory is browsable. This may be /usr/doc. See: http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1999-0678
+ /?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000: PHP reveals potentially sensitive information via certain HTTP requests that contain specific QUERY strings. See: OSVDB-12184
+ /?=PHPE9568F36-D428-11d2-A769-00AA001ACF42: PHP reveals potentially sensitive information via certain HTTP requests that contain specific QUERY strings. See: OSVDB-12184
+ /?=PHPE9568F34-D428-11d2-A769-00AA001ACF42: PHP reveals potentially sensitive information via certain HTTP requests that contain specific QUERY strings. See: OSVDB-12184
+ /?=PHPE9568F35-D428-11d2-A769-00AA001ACF42: PHP reveals potentially sensitive information via certain HTTP requests that contain specific QUERY strings. See: OSVDB-12184
+ /phpMyAdmin/changelog.php: phpMyAdmin is for managing MySQL databases, and should be protected or limited to authorized hosts.
+ /phpMyAdmin/ChangeLog: Server may leak inodes via ETags, header found with file /phpMyAdmin/ChangeLog, inode: 92462, size: 40540, mtime: Tue Dec  9 18:24:00 2008. See: http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2003-1418
+ /phpMyAdmin/ChangeLog: phpMyAdmin is for managing MySQL databases, and should be protected or limited to authorized hosts.
+ /test/: Directory indexing found.
+ /test/: This might be interesting.
+ /phpinfo.php: PHP is installed, and a test script which runs phpinfo() was found. This gives a lot of system information. See: CWE-552
+ /icons/: Directory indexing found.
+ /icons/README: Apache default file found. See: https://www.vntweb.co.uk/apache-restricting-access-to-iconsreadme/
+ /phpMyAdmin/: phpMyAdmin directory found.
+ /phpMyAdmin/Documentation.html: phpMyAdmin is for managing MySQL databases, and should be protected or limited to authorized hosts.
+ /phpMyAdmin/README: phpMyAdmin is for managing MySQL databases, and should be protected or limited to authorized hosts. See: https://typo3.org/
+ 8856 requests: 0 error(s) and 26 item(s) reported on remote host
+ End Time:           2023-04-13 11:52:57 (GMT2) (85 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested
```

Réalisme : Très réaliste. 

Durée de la capture : 100 secondes / 1.60 minute.

![http_crawling_metadata](./img/http_crawling_metadata.png)

![http_crawling_metadata_2](./img/http_crawling_metadata_2.png)



#### Brute force site web

nom pcap : **http_bruteforce_panel_dvwa.pcap**

Scénario : l'attaquant effectue une tentative de brute force sur une mire d'authentification. 

- IP attaquante : 192.168.1.57
- Adresse du site web attaquée : ctf14.root-me.org

commande lancée : 

```bash
wfuzz -c  -w ~/Documents/WORDLISTS/SecLists/Passwords/darkweb2017-top1000.txt -d "username=admin&password=FUZZ&Login=Login" -Z --sc 200 http://ctf14.root-me.org/dvwa/login.php
********************************************************
* Wfuzz 2.4.5 - The Web Fuzzer                         *
********************************************************

Target: http://ctf14.root-me.org/dvwa/login.php
Total requests: 999

===================================================================
ID           Response   Lines    Word     Chars       Payload                                                                                                                                           
===================================================================


Total time: 9.228548
Processed Requests: 999
Filtered Requests: 999
Requests/sec.: 108.2510
```

Réalisme :  Attaque réaliste et courante, génère beaucoup de bruit sur le serveur web ciblé. 

Durée de la capture : 10 secondes.

![http_bruteforce](./img/http_bruteforce.png)



![http_bruteforce_metadata](./img/http_bruteforce_metadata.png)
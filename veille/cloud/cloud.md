# Objectif de cette veille

La raison d'être de cette veille sur les menaces envers les environnements Cloud est simple : la sécurité est primordiale pour les entreprises qui utilisent des solutions Cloud.

Avec l'adoption croissante des environnements Cloud, les cybercriminels cherchent de plus en plus à exploiter les vulnérabilités des systèmes pour accéder aux données et aux applications critiques. Les menaces envers les environnements Cloud sont nombreuses et en constante évolution, ce qui rend la surveillance et la prévention de ces menaces plus importantes que jamais. 

Cette veille portera dans un premier temps sur les principaux éditeurs Cloud, à savoir AWS, Azure et GCP. Ces fournisseurs Cloud sont largement utilisés par les entreprises du monde entier, et leurs services offrent une grande variété de fonctionnalités et de capacités. Cependant, chaque plateforme a ses propres vulnérabilités et risques de sécurité, en conséqueces nous surveillerons les dernières menaces et pratiques les plsu répandues et/ou naissantes. 
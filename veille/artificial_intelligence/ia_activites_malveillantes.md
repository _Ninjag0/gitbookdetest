## IA appliquée aux activités malveillantes

- [Cybercriminals using ai pose greater security threat report](https://securityboulevard.com/2023/03/blackmamba-using-ai-to-generate-polymorphic-malware/)

  

### Contexte

L'usage de l'intelligence artificielle (IA) dans le domaine de la cybersécurité gagne en popularité en raison de son efficacité à détecter et prévenir les cyberattaques. Toutefois, les attaquants peuvent également se servir de cette technologie à leur avantage.

L'IA peut jouer un rôle important dans la sécurité numérique en donnant aux cybercriminels des moyens efficaces pour exploiter les failles de sécurité. Les criminels cherchent en permanence de nouvelles façons d'utiliser l'automatisation et l'accélération des attaques et grâce à l'IA ils peuvent aussi cibler simultanément plusieurs victimes et ainsi augmenter leur taux de réussite.



### ML vs IA

L'IA (Intelligence Artificielle) est un domaine de l'informatique qui vise à créer des machines capables de réaliser des tâches qui requièrent normalement une intelligence humaine. Cela peut inclure la reconnaissance vocale, la vision par ordinateur, le traitement du langage naturel, la planification, la résolution de problèmes, et bien plus encore.

Le ML (Machine Learning) est une sous-catégorie de l'IA qui se concentre sur la capacité d'une machine à apprendre à partir de données, plutôt que d'être explicitement programmée pour accomplir une tâche. En ML, une machine utilise des algorithmes pour analyser des données et trouver des modèles dans ces données. Les modèles appris sont ensuite utilisés pour effectuer des prévisions, des classifications ou d'autres tâches.

En résumé, l'IA est un domaine plus large qui englobe toutes les techniques qui permettent à une machine de simuler l'intelligence humaine, tandis que le ML est une technique spécifique utilisée pour enseigner à une machine à apprendre à partir de données.



- [Artificial Intelligence vs Machine Learning: Understanding the Differences](https://www.cybersecurity-insiders.com/artificial-intelligence-vs-machine-learning-understanding-the-differences/?utm_source=rss&utm_medium=rss&utm_campaign=artificial-intelligence-vs-machine-learning-understanding-the-differences).

  

### Cyberattaques et IA

Les cyberattaques améliorées par l'IA peuvent être utilisées pour lancer des attaques sophistiquées et ciblées sur les réseaux et les systèmes. Il peut être utilisé pour automatiser le processus d'identification et d'exploitation des vulnérabilités, ainsi que pour lancer des attaques par déni de service distribué (DDoS). L'IA peut également être utilisée pour créer des campagnes de phishing plus efficaces, ainsi que pour générer du code malveillant qui peut échapper aux mesures de sécurité traditionnelles.

Quelques exemples : 



- En 2018, les chercheurs d'IBM ont découvert une souche de malware hautement sophistiquée appelée [DeepLocker](https://i.blackhat.com/us-18/Thu-August-9/us-18-Kirat-DeepLocker-Concealing-Targeted-Attacks-with-AI-Locksmithing.pdf), qui utilise l'IA pour éviter la détection.

- En 2019, une f[ausse attaque audio](https://www.washingtonpost.com/technology/2019/09/04/an-artificial-intelligence-first-voice-mimicking-software-reportedly-used-major-theft/) a été signalée par Symantec, où des attaquants alimentés par l'IA ont utilisé un son convaincant de la voix d'une personne pour faire croire aux victimes qu'elles parlaient à un contact légitime.

- En 2020, Check Point a signalé un incident au cours duquel ils ont découvert un nouveau type d'attaque de phishing qui utilise de fausses vidéos de type deepfake pour se faire passer pour des cadres supérieurs.

- En 2021, Barracuda Networks a signalé un incident où des cybercriminels utilisaient des chatbots alimentés par l'IA pour mener des attaques de phishing et converser avec les victimes pour inciter à cliquer sur des liens malveillants ou divulguer des informations sensibles.

- Le 21 décembre 2022, un acteur de menace a posté sur un forum de piratage souterrain un [script de chiffrement/déchiffrement à plusieurs couches basé sur Python](https://research.checkpoint.com/2023/opwnai-cybercriminals-starting-to-use-chatgpt/), qu'il a créé avec l'aide de code openAI. Le script avait le potentiel de servir de rançongiciel et cet utilisateur en particulier a un historique d'activités illicites, notamment la vente d'accès à des organisations compromises et de bases de données volées.

- Le 29 décembre 2022, [un individu sur un forum de piratage ](https://research.checkpoint.com/2023/opwnai-cybercriminals-starting-to-use-chatgpt/)a noté qu'il expérimentait avec ChatGPT pour recréer diverses variantes de logiciels malveillants. Il a posté du code pour un voleur d'informations basé sur Python qui peut identifier, copier, compresser et exfiltrer des types de fichiers courants. 

- Le 31 décembre 2022, un acteur de menace a posté sur un forum de piratage cybercriminel sous le titre de discussion "[Abusing ChatGPT to create DarkWeb marketplaces scripts](https://research.checkpoint.com/2023/opwnai-cybercriminals-starting-to-use-chatgpt/)" . 

- Des [chercheurs des universités de Californie](https://arxiv.org/pdf/2301.02344.pdf) et de Virginie, ainsi que de Microsoft, ont développé une attaque d'empoisonnement capable de tromper les moteurs de suggestion de code tels que ChatGPT d'OpenAI et Copilot de GitHub en recommandant du code malveillant en réponse à des demandes innocentes.

  

## Domaines d'applicabilité



### Découvertes de vulnérabilités

Avec ChatGPT, les programmeurs peuvent identifier les erreurs de code de manière unique. Le chatbot fournira un résultat étonnamment précis des bogues ou des problèmes dans le code source fourni en faisant une simple demande de débogage, suivie du code en question. Malheureusement, il est également possible pour les attaquants d'utiliser cette capacité pour identifier les vulnérabilités de sécurité.

Lorsque Brendan Dolan-Gavitt a demandé au chatbot de trouver une vulnérabilité, il a fourni le code source pour résoudre le ctf. Après plusieurs questions, le bot a identifié la vulnérabilité de débordement de tampon avec une précision incroyable. En plus de fournir la solution, ChatGPT a également expliqué son processus de réflexion à des fins éducatives.

[Accès aux tweets.](https://twitter.com/moyix/status/1598081204846489600?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1598081204846489600%7Ctwgr%5Efa2ed4b2f2536af4cb5a48ed7d3c1456a340f5d7%7Ctwcon%5Es1_c10&ref_url=https%3A%2F%2Fcdn.iframe.ly%2Fvg50zH3%3Fapp%3D1)

### Ecriture d'exploits

Les chercheurs de Cybernews ont réussi à exploiter une vulnérabilité que le chatbot a trouvée en utilisant ChatGPT. Ils ont demandé au chatbot de terminer une box sur HTB, qui a réussi haut la main.

ChatGPT a fourni des instructions étape par étape sur les endroits à cibler, des échantillons de code d'exploitation et des exemples à suivre, en moins de 45 minutes, ils ont réussi à écrire une attaque pour une application connue.

* [How hackers might be exploiting ChatGPT.](https://cybernews.com/security/hackers-exploit-chatgpt/)



### Développement de malwares et diffusion

Check Point a découvert trois instances sur des forums underground où des hackers ont utilisé le chatbot pour développer des outils malveillants dans les trois semaines suivant la mise en service de ChatGPT.

Un voleur basé sur Python, par exemple, recherche des types de fichiers courants, les copie dans un dossier aléatoire dans le dossier Temp, les compresse au format ZIP et les télécharge sur un serveur FTP codé en dur. Un autre exemple est ChatGPT qui développe un programme Java qui télécharge Putty et l'exécute en arrière-plan de manière furtive en utilisant [PowerShell](https://heimdalsecurity.com/blog/researchers-warn-about-powershell-backdoor-exploited-by-hackers/).

L'équipe de [cybersécurité de CyberArk](https://www.hyas.com/blog/blackmamba-using-ai-to-generate-polymorphic-malware), qui a utilisé l'API de ChatGPT pour créer un[ malware polymorphe](https://www.esecurityplanet.com/threats/blackmamba-malware-edr-bypass/), a fourni peut-être l'exemple le plus effrayant. Ce type de malware modifie son comportement sur chaque victime pour éviter la détection basée sur les signatures.

Leur compte rendu technique explique comment ils contournent certaines des sauvegardes intégrées à la version web en intégrant une API directement dans le code Python. En conséquence, un nouveau type de malware est apparu qui évolue constamment et qui est entièrement indétectable par les logiciels antivirus traditionnels.

Egalament, l'article [*"Threat actors abuse AI-generated YouTube videos to spread stealer malware"*](https://cloudsek.com/blog/threat-actors-abuse-ai-generated-youtube-videos-to-spread-stealer-malware) publié sur le blog de CloudSEK, en avril 2021, rapporte comment les cybercriminels utilisent des vidéos générées par l'IA sur YouTube pour propager des logiciels malveillants de type "infostealer".

Selon l'article, les cybercriminels créent des vidéos générées par l'IA qui sont censées offrir des outils de piratage pour des jeux vidéo populaires. Ces vidéos sont ensuite publiées sur YouTube, où elles sont vues par un grand nombre de personnes. Les cybercriminels incluent ensuite un lien de téléchargement pour les outils de piratage dans la description de la vidéo.

Lorsque les utilisateurs cliquent sur le lien pour télécharger les outils de piratage, ils sont en fait redirigés vers un site Web qui télécharge un logiciel malveillant de type "infostealer" sur leur ordinateur. Ce type de logiciel malveillant est conçu pour voler les informations d'identification, les mots de passe et les données sensibles des utilisateurs.



### Des programmes malveillants apprenants

La particularité des technologies d'IA réside dans le fait qu’elles sont capables d’apprendre, de progresser et de s’adapter aux différentes situations. Cela n’a évidemment pas échappé aux pirates qui exploitent leurs capacités pour modéliser des attaques adaptables par le biais de programmes malveillants intelligents. Ces malwares de dernière génération sont capables de collecter des informations sur ce qui a empêché une attaque de fonctionner afin de conserver uniquement ce qui s’est avéré utile pour gagner en efficacité.

Si une attaque basée sur l’IA échoue dans un premier temps, ses capacités d’adaptabilité peuvent permettre aux pirates de réussir les attaques suivantes. De quoi donner du fil à retordre aux éditeurs de sécurité qui devront apprendre ces techniques pour pouvoir développer des outils capables de les détecter et de les contrer.

#### Phishing

Pour la création d'e-mails de phishing bien pensés à grande échelle, ChatGPT est presque indiscernable d'un humain lorsqu'il écrit et répond. De plus, le chatbot peut être utilisé pour écrire différents messages électroniques, en changeant le style d'écriture pour qu'il soit plus chaleureux et amical ou plus axé sur les affaires.

Parfois, les utilisateurs peuvent demander au chatbot d'écrire un e-mail pour eux en tant que célébrité ou personne célèbre. En fin de compte, le chatbot produit un e-mail bien écrit et soigneusement élaboré qui peut être utilisé pour des attaques de phishing.

Comparé aux véritables e-mails de phishing, souvent mal écrits avec des erreurs grammaticales, le texte fourni par ChatGPT est exceptionnellement bien écrit, permettant aux attaquants du monde entier de créer des e-mails de phishing réalistes sans erreurs de traduction.



* [AI used to create phishing templates](https://cyberwarzone.com/ai-used-to-create-phishing-templates/)
* [[Ai chatbots making it harder to spot phishing emails say experts](https://www.theguardian.com/technology/2023/mar/29/ai-chatbots-making-it-harder-to-spot-phishing-emails-say-experts)]
* [A Proof of Concept Script: The Role of ChatGPT in Creating Persuasive Phishing Attack Templates](https://cyberwarzone.com/ai-used-to-create-phishing-templates/)



#### Génération de malware polymorphe 

L'article ["BlackMamba: Using AI to Generate Polymorphic Malware"](https://securityboulevard.com/2023/03/blackmamba-using-ai-to-generate-polymorphic-malware/) publié sur Security Boulevard en mars 2023, aborde l'utilisation de l'intelligence artificielle (IA) dans la génération de logiciels malveillants polymorphes par les cybercriminels.

Selon l'article, le groupe de cybercriminels "BlackMamba" utilise une technique pour générer des logiciels malveillants polymorphes, ce qui rend plus difficile leur détection par les systèmes de sécurité informatique. Les cybercriminels utilisent l'IA pour générer automatiquement des variantes du code malveillant, qui sont uniques et différentes les unes des autres.

BlackMamba utilise un exécutable bénin qui se connecte à une API à haute réputation (OpenAI) à l'exécution, de sorte qu'il puisse renvoyer le code malveillant synthétisé nécessaire pour voler les frappes d'un utilisateur infecté. Il exécute ensuite le code généré de manière dynamique dans le contexte du programme bénin à l'aide de la fonction exec() de Python, la partie polymorphe malveillante restant entièrement en mémoire.

À l'aide de sa capacité de keylogging intégrée, BlackMamba peut collecter des informations sensibles, telles que des noms d'utilisateur, des mots de passe, des numéros de carte de crédit et d'autres données personnelles ou confidentielles que l'utilisateur tape sur son appareil. Une fois que ces données sont capturées, le malware utilise le webhook de MS Teams pour envoyer les données collectées au canal Teams malveillant, où elles peuvent être analysées, vendues sur le dark web ou utilisées à d'autres fins néfastes.



### DeepFace & Co

En 2019, une fausse attaque audio a été signalée par Symantec, où des attaquants alimentés par l'IA ont utilisé un son convaincant de la voix d'une personne pour faire croire aux victimes qu'elles parlaient à un contact légitime.

En 2020, Check Point a signalé un incident au cours duquel ils ont découvert un nouveau type d'attaque de phishing qui utilise de fausses vidéos profondes pour se faire passer pour des cadres supérieurs. Les attaquants ont exploité l'IA pour créer de fausses vidéos convaincantes de PDG ou d'autres cadres, qu'ils ont ensuite utilisées pour demander des fonds ou des informations sensibles aux employés.

* [Pausing AI Developments Isn't Enough. We Need to Shut it All Down](https://time.com/6266923/ai-eliezer-yudkowsky-open-letter-not-enough/?utm_source=gitbook&utm_medium=iframely)
